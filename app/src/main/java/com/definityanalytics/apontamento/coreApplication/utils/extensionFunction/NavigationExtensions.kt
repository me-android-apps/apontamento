package com.definityanalytics.apontamento.coreApplication.utils.extensionFunction

import android.os.Bundle
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.definityanalytics.apontamento.R

/**
 * Extension to navigate to a destination, clearing the current back stack and launching the
 * fragment as `Single Top`, from the current navigation graph. This supports both navigating via an
 * {@link NavDestination#getAction(int) action} and directly navigating to a destination.
 *
 * @param resId an [androidx.navigation.NavDestination.getAction] id or a destination id to
 *              navigate to
 * @param args arguments to pass to the destination
 */
fun NavController.navigateSingleTop(
    @IdRes resId: Int,
    args: Bundle? = null,
    @AnimRes animExit: Int? = null,
    @AnimRes animEnter: Int? = null,
    @AnimRes animPopExit: Int? = null,
    @AnimRes animPopenter: Int? = null
) {
    val hostDestinationId = graph.startDestination
    val navOptions = NavOptions.Builder()
    navOptions.apply {
        setPopUpTo(hostDestinationId, false)
        setLaunchSingleTop(true)
        animExit?.let { setExitAnim(it) } //R.anim.anim_fragment_fade_out
        animEnter?.let { setEnterAnim(it) }//R.anim.anim_fragment_to_top
        animPopExit?.let { setPopExitAnim(it) } //R.anim.anim_fragment_to_bottom
        animPopenter?.let { setPopEnterAnim(it) } //R.anim.anim_fragment_to_bottom
    }

    navigate(resId, args, navOptions.build())
}
fun NavController.navigateAnimToTop(
    @IdRes resId: Int,
    args: Bundle? = null
) {
    val navOptions = NavOptions.Builder()
    navOptions.apply {
        setLaunchSingleTop(true)
        setEnterAnim(R.anim.anim_fragment_to_top)
        setPopExitAnim(R.anim.anim_fragment_to_bottom)
        setExitAnim(R.anim.anim_fragment_fade_out)
        setPopEnterAnim(R.anim.anim_fragment_fade_in)
    }

    navigate(resId, args, navOptions.build())
}
fun NavController.navigateAnimToLeft(
    @IdRes resId: Int,
    args: Bundle? = null
) {
    val navOptions = NavOptions.Builder()
    navOptions.apply {
        setLaunchSingleTop(true)
        setEnterAnim(R.anim.anim_fragment_right_to_center)
        setExitAnim(R.anim.anim_fragment_center_to_left)
        setPopEnterAnim(R.anim.anim_fragment_left_to_center)
        setPopExitAnim(R.anim.anim_fragment_center_to_right)

    }

    navigate(resId, args, navOptions.build())
}
