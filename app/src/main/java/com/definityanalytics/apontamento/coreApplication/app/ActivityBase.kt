package com.definityanalytics.apontamento.coreApplication.app

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.definityanalytics.apontamento.R
import com.mebill.functionsgenericlibrary.getColorStateListCustom
import com.mebill.functionsgenericlibrary.pxTodp
import kotlinx.android.synthetic.main.alert_dialog_error_custom.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein


open class ActivityBase : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    private var alertDialogLoading: AlertDialog? = null
    private var alertDialogError: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("ActivityBase", "ONCREATE")
    }

    /**
     * DIALOG LOADING
     */
    fun showDialogLoading() {
        if (alertDialogLoading == null) {
            val alertBuilder = AlertDialog.Builder(this, R.style.CustomDialogTransParent)
            val inflater = LayoutInflater.from(this)
            val dialogView = inflater.inflate(R.layout.alert_dialog_loading_custom, null)
            alertBuilder.setView(dialogView)


            alertDialogLoading = alertBuilder.create()
            alertDialogLoading?.apply {
                setCancelable(false)
                setCanceledOnTouchOutside(false)

                try {
                    if (!isShowing && !this@ActivityBase.isFinishing) {
                        show()

                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            alertDialogLoading?.window?.setLayout(pxTodp(100), pxTodp(100))
        }
    }

    fun dismissDialogLoading() {
        alertDialogLoading?.let {
            try {
                if (!isFinishing && it.isShowing) {
                    it.dismiss()
                    alertDialogLoading = null
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    /**
     * DIALOG ERROR
     */
    /**
     * @param titleDialog must be a resource of the string.xml
     * @param contentDialog must be a resource of the string.xml
     *
     * Can be called without parameter, title and content will be the default
     *
     */
    fun showDialogError(@StringRes titleDialog: Int, @StringRes contentDialog: Int) {
        _showDialogError(getString(titleDialog),getString(contentDialog))
    }
    fun showDialogError(titleDialog: String, contentDialog: String) {
        _showDialogError(titleDialog,contentDialog)
    }

    @SuppressLint("RestrictedApi")
    private fun _showDialogError(title: String, body: String) {
        val alertBuilder = AlertDialog.Builder(this, R.style.CustomDialogTransParent)
        val inflater = LayoutInflater.from(this)
        val dialogView = inflater.inflate(R.layout.alert_dialog_error_custom, null)
        alertBuilder.setView(dialogView)

        dialogView.btn_confirm_dialog_error?.setOnClickListener { dismissDialogError() }
        dialogView.txt_header_dialog_error?.text = title

        dialogView.txt_body_dialog_error?.text = body
        dialogView.btn_confirm_dialog_error?.supportBackgroundTintList = getColorStateListCustom(R.color.colorPrimary)



        alertDialogError = alertBuilder.create()
        alertDialogError?.apply {
            try {
                if (!isShowing && !this@ActivityBase.isFinishing) {
                    show()

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        alertDialogError?.window?.setLayout(pxTodp(340),ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    fun dismissDialogError() {
        alertDialogError?.let {
            try {
                if (!isFinishing && it.isShowing) {
                    it.dismiss()
                    alertDialogError = null
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }
}
