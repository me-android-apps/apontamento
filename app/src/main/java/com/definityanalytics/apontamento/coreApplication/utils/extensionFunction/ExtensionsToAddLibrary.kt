package com.definityanalytics.apontamento.coreApplication.utils.extensionFunction

import android.app.Activity
import android.os.Build
import android.view.WindowManager
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

private val df_hour_minute_secondes = SimpleDateFormat("HH:mm:ss")

fun Activity.removeTranslucentStatusBar(){
    window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS or WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }
}

fun Date.getHourMinuteSecondsFormated(): String? {
    return try {
        return df_hour_minute_secondes.format(time)
    } catch (ex: Exception) {
        ex.printStackTrace()
        null
    }
}

fun Date.isSameDay(other: Date): Boolean {
    val thisCalendar = Calendar.getInstance()
    val otherCalendar = Calendar.getInstance()
    thisCalendar.time = this
    otherCalendar.time = other
    return thisCalendar.get(Calendar.YEAR) == otherCalendar.get(Calendar.YEAR)
            && thisCalendar.get(Calendar.DAY_OF_YEAR) == otherCalendar.get(Calendar.DAY_OF_YEAR)
}

fun Date.isDayBefore(): Boolean {
    val originCalendar = Calendar.getInstance()
    originCalendar.time = this

    val todayCalendar = Calendar.getInstance()

    val isBefore = originCalendar.get(Calendar.DAY_OF_YEAR) < todayCalendar.get(Calendar.DAY_OF_YEAR)
    return isBefore
}