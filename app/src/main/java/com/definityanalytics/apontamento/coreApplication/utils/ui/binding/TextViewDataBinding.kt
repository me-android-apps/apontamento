package com.definityanalytics.apontamento.coreApplication.utils.ui.binding

import android.os.Build
import android.text.InputType
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.getHourMinuteSecondsFormated
import com.mebill.functionsgenericlibrary.getDateFormated
import com.mebill.functionsgenericlibrary.getDateHourMinuteSecondsFormated
import java.text.SimpleDateFormat
import java.util.*




@BindingAdapter(value = ["app:disableKeyBoardEdtText"])
fun disableKeyBoard(editText: EditText, disableKeyBoard : Boolean){
    if(disableKeyBoard) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            editText.showSoftInputOnFocus = false
        } else {
            editText.inputType = InputType.TYPE_NULL
        }
    }
}

@BindingAdapter("app:textConverter")
fun setTextDateConverter(view: TextView, value: Date?) {
    value?.let {
        view.text = it.getDateFormated()
    }
}

@BindingAdapter("app:textResourceCustom")
fun setTextResource(view: TextView, resourceString : Int?) {
    resourceString?.let {
        view.text = view.context.getText(resourceString)
    }
}

@BindingAdapter("app:textConverterHour")
fun setTextHourConverter(view: TextView, value: Date?) {
    value?.let {
        view.text = it.getHourMinuteSecondsFormated()
    }
}