package com.definityanalytics.apontamento.coreApplication.app

import android.app.Application
import androidx.room.Room
import com.definityanalytics.apontamento.data.db.AppDatabase
import com.definityanalytics.apontamento.data.network.ServicesApiMock
import com.definityanalytics.apontamento.data.network.ServicesApiProd
import com.definityanalytics.apontamento.data.repositories.ApontamentoRepository
import com.definityanalytics.apontamento.data.repositories.CompanyRepository
import com.definityanalytics.apontamento.ui.auth.ViewModelAuth
import com.definityanalytics.apontamento.ui.main.ViewModelApontamento
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.androidCoreModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class ApontamentoApp : Application(), KodeinAware{

    override val kodein by Kodein.lazy {
        import(androidCoreModule(this@ApontamentoApp))

        //service
        bind() from singleton { ServicesApiMock() }
        bind() from singleton { ServicesApiProd() }

        //repositories
        bind() from singleton { CompanyRepository(instance()) }
        bind() from singleton { ApontamentoRepository(instance()) }

        //auth
        bind() from singleton { ViewModelAuth.AuthViewModelFactory(instance(),instance()) }
        bind() from singleton { ViewModelApontamento.ApontamentoViewModelFactory(instance(), instance()) }
    }

    override fun onCreate() {
        super.onCreate()

        //CONFIG BANCO
        database = Room.databaseBuilder(this,AppDatabase::class.java,"AppDatabase-apontamento_db")
            .allowMainThreadQueries()
            .build()
    }

    companion object{
        var database : AppDatabase?=null
    }
}