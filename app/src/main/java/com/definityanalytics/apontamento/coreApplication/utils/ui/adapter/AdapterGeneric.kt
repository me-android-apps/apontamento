package com.definityanalytics.apontamento.coreApplication.utils.ui.adapter

import android.annotation.SuppressLint
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil

class AdapterGeneric<T>(
    @LayoutRes private val layoutId : Int,
    onItemListClicked : ((view : View?, param : T) -> Unit)?=null) : DataBindingAdapter<T>(
    DiffCallback(),onItemListClicked){

    class DiffCallback<T> : DiffUtil.ItemCallback<T>(){
        override fun areItemsTheSame(
            oldItem: T,
            newItem: T
        ): Boolean = oldItem == newItem

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(
            oldItem: T,
            newItem: T
        ): Boolean = oldItem == newItem
    }

    override fun getItemViewType(position: Int): Int {

        return layoutId
    }
}