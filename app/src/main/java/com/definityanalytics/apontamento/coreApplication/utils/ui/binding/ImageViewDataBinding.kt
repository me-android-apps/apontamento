package com.definityanalytics.apontamento.coreApplication.utils.ui.binding

import android.annotation.SuppressLint
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.mebill.functionsgenericlibrary.getColorStateListCustom
import java.lang.Exception

@BindingAdapter("app:srcCompatCustom")
fun setImageResource(imageView: ImageView, resource: Int?) {
    resource?.let {
        try {
            imageView.setImageResource(resource)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}


@SuppressLint("RestrictedApi")
@BindingAdapter("app:tintStateCustom")
fun setSupportImageTintList(view: AppCompatImageView, resourceColor: Int) {
    view.apply {
        supportImageTintList = context.getColorStateListCustom(context.resources.getColor(resourceColor))
//        supportImageTintList = colorStateList
    }
}