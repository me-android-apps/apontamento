package com.definityanalytics.apontamento.coreApplication.utils.exception

import java.io.IOException

class ApiException(message: String,var code: Int?=null) : IOException(message)