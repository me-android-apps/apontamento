package com.definityanalytics.apontamento.coreApplication.utils.ui.binding

import android.annotation.SuppressLint
import android.os.Build
import android.text.InputType
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.getHourMinuteSecondsFormated
import com.mebill.functionsgenericlibrary.getColorStateListCustom
import com.mebill.functionsgenericlibrary.getDateFormated
import com.mebill.functionsgenericlibrary.getDateHourMinuteSecondsFormated
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("RestrictedApi")
@BindingAdapter("app:backgroundTintListCustom")
fun setBackgroundTintList(view: AppCompatButton, resourceColor: Int) {
    view.apply {
        val color = R.color.colorGray
        val colorP = R.color.colorPrimary
        supportBackgroundTintList = context.getColorStateListCustom(resourceColor)
    }
}