package com.definityanalytics.apontamento.coreApplication.utils.ui.binding

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("android:visibilityCustom")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}