package com.definityanalytics.apontamento.data.network

import android.util.Log
import com.definityanalytics.apontamento.coreApplication.utils.exception.ApiException
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Response
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import retrofit2.converter.gson.GsonConverterFactory


abstract class SafeApiRequest {

    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): Response<T> {
        val response = call.invoke()
        Log.d("TAG_REQUEST", "body = ${Gson().toJson(response.raw().request().body())}")
//        Log.d("TAG_REQUEST", "body = ${Gson().toJson(response.raw().request())}")


        Log.d("TAG_RESPONSE", response.toString())
//        val auth: String? = response.headers()["Authorization"]
//        Log.d("TAG_RESPONSE_AUTH", auth ?: "NO-AUTH")

        if (response.isSuccessful) {
            return response
        } else {
            throw ApiException(response.message(), response.code())
        }
    }
}