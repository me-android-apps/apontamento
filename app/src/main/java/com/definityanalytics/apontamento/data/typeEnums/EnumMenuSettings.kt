package com.definityanalytics.apontamento.data.typeEnums

enum class EnumMenuSettings constructor(var valor : String){
    SA("Sair"),
    PP("Poíticas de Privacidade"),
    SO("Sobre")

}