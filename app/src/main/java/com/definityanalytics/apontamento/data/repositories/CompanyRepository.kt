package com.definityanalytics.apontamento.data.repositories

import com.definityanalytics.apontamento.coreApplication.app.ApontamentoApp
import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.network.SafeApiRequest
import com.definityanalytics.apontamento.data.network.ServicesApiMock
import com.definityanalytics.apontamento.data.network.ServicesApiProd
import retrofit2.Response

class CompanyRepository(private val servicesApiProd: ServicesApiProd) : SafeApiRequest() {

    suspend fun authKeyCompany(company: Company): Response<Company>?{
        val response = apiRequest {
            servicesApiProd.authKey(company.keyAcess?:"")
        }
        return response
    }

    suspend fun authIdCompany(company: Company): Response<Company>?{
        val response = apiRequest {
            servicesApiProd.authKey(company.keyAcess?.toInt()?:0)
        }
        return response
    }

    suspend fun saveLogin(company: Company): Long? {
        company.idDbLocal = 1
        val idSave = ApontamentoApp.database?.companyDao()?.insert(company.toLocalDbClass())
        return idSave
    }

    fun getUserLogado() : Company? {
        return ApontamentoApp.database?.companyDao()?.getParceiroUser()?.restoreLocaDbClass()
    }

    suspend fun logoutUserDb() {
        ApontamentoApp.database?.apply {
            companyDao().deleteParceiroUser()
            apontamentoDao().deleteAll()
        }

    }
}