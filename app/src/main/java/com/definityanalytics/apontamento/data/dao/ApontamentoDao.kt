package com.definityanalytics.apontamento.data.dao

import androidx.room.*
import com.definityanalytics.apontamento.data.model.entities.Apontamento
import com.definityanalytics.apontamento.data.model.local.ApontamentoLC

@Dao
interface ApontamentoDao {

    @Query("SELECT * FROM apontamentolc")
    suspend fun getAll(): List<ApontamentoLC>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(apontamento: ApontamentoLC):Long

    @Update
    suspend fun update(apontamento: ApontamentoLC) : Int

    @Query("DELETE FROM apontamentolc")
    suspend fun deleteAll()

    @Query("DELETE FROM apontamentolc WHERE apontamentoId LIKE :id")
    suspend fun deleteById(id : Long)
}