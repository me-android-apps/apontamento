package com.definityanalytics.apontamento.data.typeEnums

enum class EnumStatusCompany constructor(var valor : String){
    A("Ativo"),
    I("Inativo"),
    D("Devedor")

}