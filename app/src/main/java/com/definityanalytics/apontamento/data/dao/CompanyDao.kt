package com.definityanalytics.apontamento.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.model.local.CompanyLC

@Dao
interface CompanyDao {

    @Query("SELECT * FROM companylc WHERE id = 1")
    fun getParceiroUser() : CompanyLC

    @Query("SELECT keyAcess FROM companylc WHERE id = 1")
    fun getKeyAccessUser() : String

    @Query("DELETE FROM companylc WHERE id = 1")
    fun deleteParceiroUser()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(company: CompanyLC):Long
}