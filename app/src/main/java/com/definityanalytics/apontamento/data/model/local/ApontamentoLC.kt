package com.definityanalytics.apontamento.data.model.local

import androidx.databinding.ObservableField
import androidx.room.*
import com.definityanalytics.apontamento.data.model.entities.Apontamento
import com.definityanalytics.apontamento.data.model.entities.BaseEntitie
import com.definityanalytics.apontamento.data.model.entities.User
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusProcess
import com.definityanalytics.apontamento.data.typeEnums.EnumSyncStatusProcess
import com.google.gson.annotations.SerializedName
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties
import java.util.*

@Entity
class ApontamentoLC(
    @PrimaryKey
    var apontamentoId:Long?= null,

    @Embedded
    var employe : UserLC?=null,

    @Embedded
    var process : ProcessLC?=null,

    @Embedded
    var subProcess : SubProcessLC?=null,

    @ColumnInfo(name = "date_stop")
    var dateStop: Date?= null,

    var dateStart : Date?= null,

    @field:SerializedName("bar_code")
    var barCode: String?=null,

    @field:SerializedName("block_options")
    var blockOptions: Boolean?=null,

    var statusProcess : EnumStatusProcess?=null,

    var statusSyncProcess : EnumSyncStatusProcess?=null

    ):BaseLocal(){

    override fun restoreLocaDbClass(): Apontamento {
        val apontamento = Apontamento()
        apontamento.let {
            it.apontamentoId = apontamentoId
            it.employe.set(employe?.restoreLocaDbClass())
            it.process.set(process?.restoreLocaDbClass())
            it.subProcess.set(subProcess?.restoreLocaDbClass())
            it.dateStart.set(dateStart)
            it.dateStop.set(dateStop)
            it.barCode.set(barCode)
            it.blockOptions = blockOptions
            it.statusProcess.set(statusProcess)
            it.statusSyncProcess.set(statusSyncProcess)
        }

        return apontamento
    }
}