package com.definityanalytics.apontamento.data.db

import androidx.databinding.ObservableField
import androidx.room.TypeConverter
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusProcess
import com.definityanalytics.apontamento.data.typeEnums.EnumSyncStatusProcess
import java.util.*

class Converters{

    @TypeConverter
    fun dateToLong(value: Date?) = value?.time

    @TypeConverter
    fun fromDate(value: Long?) = value?.let{ Date(it) }



    /**
     * ENUMS
     */
    @TypeConverter fun enumStatusProcessToTnt(value: EnumStatusProcess?) = value?.toInt()

    @TypeConverter fun fromEnumStatusProcess(value: Int?) = value?.toEnum<EnumStatusProcess>()

    @TypeConverter fun enumStatusSyncProcessToTnt(value: EnumSyncStatusProcess?) = value?.toInt()

    @TypeConverter fun fromEnumStatusSyncProcess(value: Int?) = value?.toEnum<EnumSyncStatusProcess>()

    @Suppress("NOTHING_TO_INLINE")
    private inline fun <T : Enum<T>> T.toInt(): Int = this.ordinal

    private inline fun <reified T : Enum<T>> Int.toEnum(): T = enumValues<T>()[this]
}