package com.definityanalytics.apontamento.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.definityanalytics.apontamento.data.dao.ApontamentoDao
import com.definityanalytics.apontamento.data.dao.CompanyDao
import com.definityanalytics.apontamento.data.model.entities.Apontamento
import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.model.local.ApontamentoLC
import com.definityanalytics.apontamento.data.model.local.CompanyLC

/**
 * The Room database for this app
 */

@Database(version = 1,
    entities = [
        CompanyLC::class,
        ApontamentoLC::class
    ])
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun companyDao() : CompanyDao
    abstract fun apontamentoDao() : ApontamentoDao
}
