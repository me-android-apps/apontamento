package com.definityanalytics.apontamento.data.repositories

import com.definityanalytics.apontamento.coreApplication.app.ApontamentoApp
import com.definityanalytics.apontamento.data.model.entities.Apontamento
import com.definityanalytics.apontamento.data.model.entities.SubProcess
import com.definityanalytics.apontamento.data.model.entities.User
import com.definityanalytics.apontamento.data.model.entities.Process
import com.definityanalytics.apontamento.data.network.SafeApiRequest
import com.definityanalytics.apontamento.data.network.ServicesApiProd
import retrofit2.Response

class ApontamentoRepository(private val servicesApiProd: ServicesApiProd) : SafeApiRequest() {

    suspend fun getAllApontamentos(): List<Apontamento>? {
        val listApontamento = ArrayList<Apontamento>()

        ApontamentoApp.database?.apontamentoDao()?.getAll()?.forEach { apontamentoLC ->
            listApontamento.add(apontamentoLC.restoreLocaDbClass())
        }
      return listApontamento
    }

    suspend fun createApontamento(apontamento: Apontamento):Long? {
        return ApontamentoApp.database?.apontamentoDao()?.insert(apontamento.toLocalDbClass())
    }

    suspend fun dellListApontamento(listApontamento : List<Apontamento>) {
        listApontamento.forEach { ap ->
            ApontamentoApp.database?.apontamentoDao()?.deleteById(ap.apontamentoId!!)
        }
    }

    suspend fun updateApontamento(apontamento: Apontamento) {
        ApontamentoApp.database?.apontamentoDao()?.insert(apontamento.toLocalDbClass())
    }

    suspend fun getEmployees(id: Int): Response<List<User>>? {
        val response = apiRequest {
            servicesApiProd.getEmployees(empresaId = id)
        }
        return response
    }

    suspend fun getProcess(id: Int): Response<List<Process>>? {
        val response = apiRequest {
            servicesApiProd.getProcess(empresaId = id)
        }
        return response
    }

    suspend fun getSubProcess(idProcess : Int): Response<List<SubProcess>>? {
        val response = apiRequest {
            servicesApiProd.getSubProcess(idProcess)
        }
        return response
    }

    suspend fun postRelease(apontamento: Apontamento): Response<Unit> {
        val company = ApontamentoApp.database?.companyDao()?.getParceiroUser()?.restoreLocaDbClass()
        val release = apontamento.toServerClass()
        release.apply {
            EmpresaID = company?.id
        }

        val response = apiRequest {
            servicesApiProd.postRelease(release)
        }
        return response
    }


}