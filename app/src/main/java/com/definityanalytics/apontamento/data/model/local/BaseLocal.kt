package com.definityanalytics.apontamento.data.model.local

import com.definityanalytics.apontamento.data.model.entities.BaseEntitie

abstract class BaseLocal {
    abstract fun restoreLocaDbClass() : BaseEntitie
}