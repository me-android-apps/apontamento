package com.definityanalytics.apontamento.data.model.local

import androidx.room.ColumnInfo
import com.definityanalytics.apontamento.data.model.entities.User
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class UserLC(
    @JsonIgnore
    var userId:Long?= null,
    @ColumnInfo(name= "name_user")
    var name:String?=null,
    @ColumnInfo(name= "id_store")
    var storeId:Int?=null,
    @ColumnInfo(name= "id_employee")
    var employeeId:Int?=null
):BaseLocal(){

    override fun restoreLocaDbClass(): User {
        val user = User()
        user.let {
            it.userId = userId
            it.NomeFuncionario = name
            it.LojaID = storeId
            it.FuncionarioID = employeeId
        }

        return user
    }
}