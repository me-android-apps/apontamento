package com.definityanalytics.apontamento.data.model.server

data class Lancamento(
    var EmpresaID: Int? = null,
    var Cdbar: String?= null,
    var ProcessoID: Int? = null,
    var ProcessoSubID: Int? = null,
    var DataInicio: String? = null,
    var DataFim: String? = null,
    var HoraInicio: String? = null,
    var HoraFim: String? = null,
    var LojaID: Int? = null,
    var FuncionarioID: Int? = null,
    var Pendencia: Boolean? = null,
    var Response: String? = null
)