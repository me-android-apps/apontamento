package com.definityanalytics.apontamento.data.model.entities

import androidx.databinding.ObservableField
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.getHourMinuteSecondsFormated
import com.definityanalytics.apontamento.data.model.local.ApontamentoLC
import com.definityanalytics.apontamento.data.model.local.ProcessLC
import com.definityanalytics.apontamento.data.model.server.Lancamento
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusProcess
import com.definityanalytics.apontamento.data.typeEnums.EnumSyncStatusProcess
import com.google.gson.annotations.SerializedName
import com.mebill.functionsgenericlibrary.getDateFormated
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class Apontamento(
    var apontamentoId: Long? = null,

    var employe: ObservableField<User> = ObservableField<User>(),

    var process: ObservableField<Process> = ObservableField<Process>(),

    var subProcess: ObservableField<SubProcess> = ObservableField<SubProcess>(),

    var dateStart: ObservableField<Date> = ObservableField<Date>(),

    var dateStop: ObservableField<Date> = ObservableField<Date>(),

    var barCode: ObservableField<String> = ObservableField<String>(),

    var blockOptions: Boolean? = null,

    @JsonIgnore
    var statusProcess: ObservableField<EnumStatusProcess> = ObservableField<EnumStatusProcess>(),

    @JsonIgnore
    var statusSyncProcess: ObservableField<EnumSyncStatusProcess> = ObservableField<EnumSyncStatusProcess>()
) : BaseEntitie() {

    override fun baseToString(): String {
        return ""
    }

    override fun toLocalDbClass(): ApontamentoLC {
        val apontamentoLC = ApontamentoLC()
        apontamentoLC.let {
            it.apontamentoId = apontamentoId
            it.employe = employe.get()?.toLocalDbClass()
            it.process = process.get()?.toLocalDbClass()
            it.subProcess = subProcess.get()?.toLocalDbClass()
            it.dateStart = dateStart.get()
            it.dateStop = dateStop.get()
            it.barCode = barCode.get()
            it.blockOptions = blockOptions
            it.statusProcess = statusProcess.get()
            it.statusSyncProcess = statusSyncProcess.get()
        }

        return apontamentoLC
    }

    fun toServerClass(): Lancamento {
        val lancamento = Lancamento()
        lancamento.let {
            it.Cdbar = barCode.get()
            it.ProcessoID = process.get()?.ProcessoID
            it.ProcessoSubID = subProcess.get()?.SubProcessoID ?: 0
            it.LojaID = employe.get()?.LojaID
            it.FuncionarioID = employe.get()?.FuncionarioID
            it.DataInicio = dateStart.get()?.getDateFormated()
            it.HoraInicio = dateStart.get()?.getHourMinuteSecondsFormated()
            it.DataFim = dateStop.get()?.getDateFormated()
            it.HoraFim = dateStop.get()?.getHourMinuteSecondsFormated()
        }
        return lancamento
    }
}