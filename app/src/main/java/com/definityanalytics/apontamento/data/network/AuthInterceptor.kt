package com.definityanalytics.apontamento.data.network

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.ConnectException

/**
 * A {@see RequestInterceptor} that adds an auth token to requests
 */
class AuthInterceptor(private val accessToken: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val request = chain.request().newBuilder().addHeader(
                "authorization", accessToken).build()
            val response = chain.proceed(request)
            return response
        }catch (ex : IOException){
            ex.printStackTrace()
            throw ex
        }catch (ex : ConnectException){
            ex.printStackTrace()
            throw ex
        }
    }

}
