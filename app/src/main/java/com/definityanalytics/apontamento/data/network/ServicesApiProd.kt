package com.definityanalytics.apontamento.data.network

import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.model.entities.SubProcess
import com.definityanalytics.apontamento.data.model.entities.User
import com.definityanalytics.apontamento.data.model.entities.Process
import com.definityanalytics.apontamento.data.model.server.Lancamento
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServicesApiProd {
    companion object {
        const val PROD_ENDPOINT = "http://definityanalytics.com.br:5595/api/api/"

        operator fun invoke(): ServicesApiProd {
            return Retrofit.Builder()
                .baseUrl(PROD_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
//                .client(provideOkHttpClient())
                .build()
                .create(ServicesApiProd::class.java)
        }

        private fun provideOkHttpClient(): OkHttpClient {
            val okhttpClientBuilder = OkHttpClient.Builder()
            okhttpClientBuilder.addInterceptor(AuthInterceptor(""/*sharedPreferences?.tokenServicesApi*/))

            return okhttpClientBuilder.build()

        }
    }

    @GET("Empresas")
    suspend fun authKey(@Query("pKey") keyAcess: String) : Response<Company>//EmpresaID=1467

    @GET("Empresas")
    suspend fun authKey(@Query("EmpresaID") idCompany: Int) : Response<Company>//EmpresaID=1467

    @GET("Funcionarios")
    suspend fun getEmployees(@Query("EmpresaID") empresaId : Int) : Response<List<User>>//id=2&empid=1

    @GET("Processos")
    suspend fun getProcess(@Query("EmpresaID") empresaId: Int) : Response<List<Process>>//id=2&empid=1

    @GET("ProcessosSub")
    suspend fun getSubProcess(@Query("ProcessoID") processoId: Int) : Response<List<SubProcess>>//id=2&empid=1

    @POST("Lancamentos")
    suspend fun postRelease(@Body release : Lancamento) : Response<Unit>



}