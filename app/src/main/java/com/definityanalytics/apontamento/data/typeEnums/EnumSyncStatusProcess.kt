package com.definityanalytics.apontamento.data.typeEnums

enum class EnumSyncStatusProcess constructor(var valor : String){
    SY("Sincronizado com ws"),
    DS("Não Sincronizado com ws")
}