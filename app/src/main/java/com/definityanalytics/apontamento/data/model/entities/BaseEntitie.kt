package com.definityanalytics.apontamento.data.model.entities

import com.definityanalytics.apontamento.data.model.local.BaseLocal

abstract class BaseEntitie {

    abstract fun baseToString():String

    abstract fun toLocalDbClass():BaseLocal
}