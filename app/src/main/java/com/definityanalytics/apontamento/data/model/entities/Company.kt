package com.definityanalytics.apontamento.data.model.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.definityanalytics.apontamento.data.model.local.BaseLocal
import com.definityanalytics.apontamento.data.model.local.CompanyLC
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusCompany
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusProcess
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties

/*
          "id": 1467,
  "cnpj": null,
  "nomeempresa": "FARMA VIDA FARMACIA",
  "business": "320-321-425",
  "situacao": "A",
  "ipserver": "fvida.no-ip.biz",
  "caminhobase": "d:\\fcerta\\db",
  "porta": "3306      ",
  "funcionarios": 6,
  "meta": 275000.0,
  "horas": 10,
  "dias": 22.0,
  "filial": "1"
 */
data class Company(
    @JsonIgnore
    var idDbLocal:Int?= null,
    @JsonIgnore
    var keyAcess:String?=null,
    var id:Int?= null,
    var cnpj:Int?= null,
    var nomeempresa:String?=null,
    var business:String?=null,
    var situacao:EnumStatusCompany?=null,
    var ipserver:String?=null,
    var caminhobase:String?=null,
    var porta:String?=null,
    var funcionarios:Int?=null,
    var meta:Double?=null,
    var horas:Int?=null,
    var dias:Double?=null,
    var filial:String?=null
):BaseEntitie(){
    override fun baseToString(): String {
        return ""
    }

    override fun toLocalDbClass(): CompanyLC {
        val companyLC = CompanyLC()
        companyLC.let {
            it.id = idDbLocal
            it.keyAcess = keyAcess
            it.idCompany = id
        }
        return companyLC
    }
}