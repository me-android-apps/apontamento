package com.definityanalytics.apontamento.data.model.local

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import org.codehaus.jackson.annotate.JsonIgnoreProperties
import com.definityanalytics.apontamento.data.model.entities.Process
@JsonIgnoreProperties(ignoreUnknown = true)
class ProcessLC(
    @field:SerializedName("process_id")
    var processId:Int?= null,
    @field:SerializedName("empresa_id")
    var empresaId:Int?= null,
    @ColumnInfo(name= "name_process")
    var name:String?=null,
    @ColumnInfo(name= "sub_process")
    var subProcess:String?=null,
    @ColumnInfo(name= "position")
    var position:Int?=null,
    @ColumnInfo(name= "status")
    var status:String?=null,
    @ColumnInfo(name= "time")
    var time:Int?=null,
    @ColumnInfo(name= "final")
    var final:String?=null
):BaseLocal(){

    override fun restoreLocaDbClass(): Process {
        val process = Process()
        process.let {
            it.ProcessoID = processId
            it.Descricao = name
            it.SubItem = subProcess
            it.EmpresaID = empresaId
            it.Posicao = position
            it.Status = status
            it.Tempo = time
            it.Final = final
        }

        return process
    }
}