package com.definityanalytics.apontamento.data.model.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.definityanalytics.apontamento.data.model.entities.BaseEntitie
import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusCompany
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties

@Entity
data class CompanyLC(
    @PrimaryKey
    var id:Int?= null,

    @ColumnInfo(name = "keyAcess")
    var keyAcess:String?=null,

    var idCompany:Int?= null
):BaseLocal(){
    override fun restoreLocaDbClass(): Company {
        val company = Company()
        company.let {
            it.idDbLocal = id
            it.keyAcess = keyAcess
            it.id = idCompany
        }
        return company
    }
}