package com.definityanalytics.apontamento.data.model.entities

import androidx.room.ColumnInfo
import com.definityanalytics.apontamento.data.model.local.BaseLocal
import com.definityanalytics.apontamento.data.model.local.ProcessLC
import com.definityanalytics.apontamento.data.model.local.SubProcessLC
import com.google.gson.annotations.SerializedName
import org.codehaus.jackson.annotate.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class SubProcess(
    var SubProcessoID:Int?= null,
    var ProcessoID:Int?= null,
    var Descricao:String?=null,
    var Posicao:Int?=null,
    var Status:String?=null,
    var Tempo:Int?=null
): BaseEntitie() {
    override fun baseToString(): String {
        return Descricao?:""
    }

    override fun toLocalDbClass(): SubProcessLC {
        val subProcessLC = SubProcessLC()
        subProcessLC.let {
            it.subProcessId = SubProcessoID
            it.processId = ProcessoID
            it.name = Descricao
            it.position = Posicao
            it.status = Status
            it.time = Tempo
        }

        return subProcessLC
    }
}