package com.definityanalytics.apontamento.data.network

import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.model.entities.User
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServicesApiMock {
    companion object {
        const val MOCK_ENDPOINT = "https://my-json-server.typicode.com/joaomt/Repo-Mock-Requests/"

        operator fun invoke(): ServicesApiMock {
            return Retrofit.Builder()
                .baseUrl(MOCK_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().serializeNulls().create()))
//                .client(provideOkHttpClient())
                .build()
                .create(ServicesApiMock::class.java)
        }

        private fun provideOkHttpClient(): OkHttpClient {
            val okhttpClientBuilder = OkHttpClient.Builder()
            okhttpClientBuilder.addInterceptor(AuthInterceptor(""/*sharedPreferences?.tokenServicesApi*/))

            return okhttpClientBuilder.build()

        }
    }

    @GET("profile")//mock request
    suspend fun authKey(@Query("keyAcess") keyAcess: String) : Response<Company>

    @GET("Funcionarios")
    suspend fun getEmployees(@Query("id") id: Int,@Query("empid") empid : Int) : Response<List<User>>//id=2&empid=1

}