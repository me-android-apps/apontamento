package com.definityanalytics.apontamento.data.typeEnums

enum class EnumStatusProcess constructor(var valor : String){
    NS("Não Iníciado"),
    ST("Iníciado"),
    SO("Finalizado")

}