package com.definityanalytics.apontamento.data.model.local

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import org.codehaus.jackson.annotate.JsonIgnoreProperties
import com.definityanalytics.apontamento.data.model.entities.Process
import com.definityanalytics.apontamento.data.model.entities.SubProcess

@JsonIgnoreProperties(ignoreUnknown = true)
class SubProcessLC(
    @field:SerializedName("sub_process_id")
    var subProcessId:Int?= null,
    @ColumnInfo(name= "process_id_sub")
    var processId:Int?= null,
    @ColumnInfo(name= "name_sub_process")
    var name:String?=null,
    @ColumnInfo(name= "position_sub_process")
    var position:Int?=null,
    @ColumnInfo(name= "status_sub_process")
    var status:String?=null,
    @ColumnInfo(name= "time_sub_process")
    var time:Int?=null
):BaseLocal(){

    override fun restoreLocaDbClass(): SubProcess {
        val subProcess = SubProcess()
        subProcess.let {
            it.SubProcessoID = subProcessId
            it.ProcessoID = processId
            it.Descricao = name
            it.Posicao = position
            it.Status = status
            it.Tempo = time
        }

        return subProcess
    }
}