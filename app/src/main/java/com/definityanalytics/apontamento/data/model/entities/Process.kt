package com.definityanalytics.apontamento.data.model.entities

import androidx.room.ColumnInfo
import com.definityanalytics.apontamento.data.model.local.BaseLocal
import com.definityanalytics.apontamento.data.model.local.ProcessLC
import com.google.gson.annotations.SerializedName
import org.codehaus.jackson.annotate.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Process(
    var ProcessoID:Int?= null,
    var EmpresaID:Int?= null,
    var Descricao:String?=null,
    var SubItem:String?=null,
    var Posicao:Int?=null,
    var Status:String?=null,
    var Tempo:Int?=null,
    var Final:String?=null
): BaseEntitie() {
    override fun baseToString(): String {
        return Descricao?:""
    }

    override fun toLocalDbClass(): ProcessLC {
        val processLC = ProcessLC()
        processLC.let {
            it.processId = ProcessoID
            it.empresaId = EmpresaID
            it.name = Descricao
            it.subProcess = SubItem
            it.position = Posicao
            it.status = Status
            it.time = Tempo
            it.final = Final
        }

        return processLC
    }
}