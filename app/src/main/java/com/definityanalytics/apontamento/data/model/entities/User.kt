package com.definityanalytics.apontamento.data.model.entities

import com.definityanalytics.apontamento.data.model.local.UserLC
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class User(
    @JsonIgnore
    var userId:Long?= null,
    var NomeFuncionario:String?=null,
    var LojaID:Int?= null,
    var FuncionarioID:Int?= null): BaseEntitie() {
    override fun baseToString(): String {
        return NomeFuncionario?:""
    }

    override fun toLocalDbClass() : UserLC {
        val userLC = UserLC()
        userLC.let {
            it.userId = userId
            it.name = NomeFuncionario
            it.storeId = LojaID
            it.employeeId = FuncionarioID
        }

        return userLC
    }
}