package com.definityanalytics.apontamento.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.databinding.FragmentSplashScreenBinding
import com.definityanalytics.apontamento.di.injectViewModel
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.definityanalytics.apontamento.coreApplication.app.ActivityBase
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.navigateAnimToLeft
import org.kodein.di.generic.instance
import com.definityanalytics.apontamento.ui.auth.ViewModelAuth.AuthViewModelFactory

class FragmentSplashScreen : ScopedFragment() {
    private val factory : AuthViewModelFactory by instance<AuthViewModelFactory>()
    private lateinit var viewModelAuth : ViewModelAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentSplashScreenBinding.inflate(inflater,container,false)
        context ?: return binding.root

        viewModelAuth = activity?.injectViewModel(factory)!!

        binding.apply {
            subScribeUi(this)
        }

        viewModelAuth.checkUserLogged()
        return binding.root
    }
    //COMPONENTS
    private fun subScribeUi(bindingFrag: FragmentSplashScreenBinding) {
        viewModelAuth.activityToStart.observe(activity!!, Observer { value ->
            val intent = Intent(activity, value.first)
            value.second?.let {
                intent.putExtras(it)
            }
            startActivity(intent)
            activity?.finish()
        })
        viewModelAuth.fragmentToStart.observe(viewLifecycleOwner, Observer { idFrag ->
            Navigation.findNavController(view!!).navigateAnimToLeft(idFrag)
        })

        viewModelAuth.showDialogError.observe(viewLifecycleOwner, Observer {
            it?.let {
                (activity as ActivityBase).showDialogError(it.first,it.second)
            }
        })
    }
}