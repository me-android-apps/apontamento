package com.definityanalytics.apontamento.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ActivityBase
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.coreApplication.utils.ui.adapter.AdapterGeneric
import com.definityanalytics.apontamento.data.model.entities.BaseEntitie
import com.definityanalytics.apontamento.databinding.FragmentListGenericBinding
import com.definityanalytics.apontamento.di.injectViewModel
import org.kodein.di.generic.instance
import com.definityanalytics.apontamento.ui.main.ViewModelApontamento.ApontamentoViewModelFactory

class FragmentListGeneric : ScopedFragment() {
    //viewModel
    private val factory : ApontamentoViewModelFactory by instance<ApontamentoViewModelFactory>()
    private lateinit var viewModelApontamento: ViewModelApontamento

    private lateinit var adapterItensGeneric : AdapterGeneric<BaseEntitie>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentListGenericBinding.inflate(inflater,container,false)
        context ?: return binding.root
        binding.lifecycleOwner = this

        viewModelApontamento = activity?.injectViewModel(factory)!!

        binding.apply{
            viewmodel = viewModelApontamento
            subScribeUi(this)
            initComponents(this)
        }

        return binding.root
    }

    //COMPONENTS
    private fun subScribeUi(bindingFrag : FragmentListGenericBinding){
        viewModelApontamento.listFragListGeneric.observe(viewLifecycleOwner, Observer { list ->
            adapterItensGeneric.submitList(list)
        })
        viewModelApontamento.onBackFrag.observe(viewLifecycleOwner, Observer { onBack ->
            activity?.onBackPressed()
        })

        viewModelApontamento.loadingRequest.observe(viewLifecycleOwner, Observer { loading ->
            if (loading && activity is ActivityBase) {
                (activity as ActivityBase).showDialogLoading()
            } else if(!loading && activity is ActivityBase){
                (activity as ActivityBase).dismissDialogLoading()
            }
        })


    }

    private fun initComponents(bindingFrag : FragmentListGenericBinding){
        bindingFrag.apply {
            //img close fragment
            imgCloseFragmentListGeneric.setOnClickListener { activity?.onBackPressed() }

            rcItensFragmentListGeneric.apply {
                adapterItensGeneric = AdapterGeneric(
                    layoutId = R.layout.item_list_drop_down_generic,
                    onItemListClicked = viewModelApontamento::itemFragListGenericSelected)
                adapter = adapterItensGeneric
                adapterItensGeneric.submitList(viewModelApontamento.listFragListGeneric.value)
            }
        }
    }
}