package com.definityanalytics.apontamento.ui.auth

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.*
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.SingleLiveEvent
import com.definityanalytics.apontamento.coreApplication.utils.coroutines.Coroutines
import com.definityanalytics.apontamento.coreApplication.utils.exception.ApiException
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.isDayBefore
import com.definityanalytics.apontamento.data.model.entities.Company
import com.definityanalytics.apontamento.data.repositories.ApontamentoRepository
import com.definityanalytics.apontamento.data.repositories.CompanyRepository
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusCompany
import com.definityanalytics.apontamento.ui.main.ActivityHostMain
import com.mebill.functionsgenericlibrary.closeSoftInput
import java.util.*

class ViewModelAuth(
    private val companyRepository: CompanyRepository,
    private val repoApontamento: ApontamentoRepository
) : ViewModel(),
    LifecycleObserver {
    private val pairLogin = Pair(ActivityHostAuth::class.java, null)
    private val pairHome = Pair(ActivityHostMain::class.java, null)
    private var isAuthSplashScreen = false
    val keyCompany : String?
        get() = companyRepository.getUserLogado()?.keyAcess?:"-"

    private val _company = MutableLiveData(Company())
    private val _loadingRequest = MutableLiveData<Boolean>()
    private val _showDialogError = SingleLiveEvent<Pair<@StringRes Int, @StringRes Int>?>()
    private val _setErrorKeyEmpty = MutableLiveData<Boolean>()
    private val _fragmentToStart = SingleLiveEvent<@IdRes Int>()
    private val _activityToStart = SingleLiveEvent<Pair<Class<*>, Bundle?>>()

    val activityToStart: LiveData<Pair<Class<*>, Bundle?>>
        get() = _activityToStart

    val company: LiveData<Company>
        get() = _company

    val loadingRequest: LiveData<Boolean>
        get() = _loadingRequest

    val showDialogError: LiveData<Pair<Int, Int>?>
        get() = _showDialogError

    val setErrorKeyEmpty: LiveData<Boolean>
        get() = _setErrorKeyEmpty

    val fragmentToStart: LiveData<Int>
        get() = _fragmentToStart

    fun submitSaveKey(view: View) {
        isAuthSplashScreen = false
        view.closeSoftInput()
        if (_company.value?.keyAcess.isNullOrEmpty()) {
            _setErrorKeyEmpty.value = true
        } else {
            _setErrorKeyEmpty.value = false
            requestAuthKey()
        }
    }

    private fun requestAuthKey() {
        _loadingRequest.value = true
        Coroutines.main {
            try {
                val authKeyCompanyResponse = company.value?.let {
                    if (it.keyAcess?.isDigitsOnly() ?: false) {
                        companyRepository.authIdCompany(it)
                    } else {
                        companyRepository.authKeyCompany(it)//320-321-425
                    }
                }
                _loadingRequest.value = false

                authKeyCompanyResponse?.let {
                    if (it.isSuccessful) {
                        when (it.body()?.situacao) {
                            EnumStatusCompany.A -> {
                                it.body()?.let { companyResponse ->
                                    companyResponse.keyAcess = company.value?.keyAcess
                                    val idSaved = companyRepository.saveLogin(companyResponse)
                                    _activityToStart.value = pairHome
                                }
                            }
                            EnumStatusCompany.I -> _showDialogError.value =
                                Pair(R.string.company_inactive, R.string.check_with_adm_system)
                            EnumStatusCompany.D -> _showDialogError.value = Pair(
                                R.string.pendency_company_money,
                                R.string.check_with_adm_system
                            )
                            null -> _showDialogError.value =
                                Pair(R.string.data_company_invalid, R.string.check_key_company)
                        }

                    } else {
                        if (isAuthSplashScreen) {
                            isAuthSplashScreen = false
                            _fragmentToStart.value = R.id.action_splashFragment_to_keyAccessFragment
                        } else {
                            _showDialogError.value =
                                Pair(R.string.key_company_invalid, R.string.check_key_company)
                        }
                    }
                }

            } catch (e: ApiException) {
                if (isAuthSplashScreen) {
                    isAuthSplashScreen = false
                    _fragmentToStart.value = R.id.action_splashFragment_to_keyAccessFragment
                } else {
                    _loadingRequest.value = false
                    _showDialogError.value =
                        Pair(R.string.key_company_invalid, R.string.check_key_company)
                }
            } catch (e: Exception) {
                if (isAuthSplashScreen) {
                    isAuthSplashScreen = false
                    _fragmentToStart.value = R.id.action_splashFragment_to_keyAccessFragment
                } else {
                    _loadingRequest.value = false
                    _showDialogError.value =
                        Pair(R.string.title_error_auth, R.string.body_check_connect)
                }
            }
        }
    }

    fun checkUserLogged() {
        isAuthSplashScreen = true
        _company.value = companyRepository.getUserLogado()
        if (_company.value?.keyAcess.isNullOrEmpty()) {
            Handler().postDelayed({
                _company.value = Company()
                _fragmentToStart.value = R.id.action_splashFragment_to_keyAccessFragment
            }, 2000)
        } else {
            Coroutines.main {
                val response = repoApontamento.getAllApontamentos()
                val listToDell = response?.filter { apontamento ->
                    apontamento.dateStop.get()?.isDayBefore() ?: false
                }
                listToDell?.let { repoApontamento.dellListApontamento(listApontamento = it) }

                requestAuthKey()
            }
        }
    }

    fun logoutUser() {
        Coroutines.main {
            companyRepository.logoutUserDb()
        }
        _activityToStart.value = pairLogin
        _company.value = Company()

    }

    @Suppress("UNCHECKED_CAST")
    class AuthViewModelFactory(
        private val companyRepository: CompanyRepository,
        private val repoApontamento: ApontamentoRepository
    ) :
        ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ViewModelAuth(companyRepository, repoApontamento) as T
        }
    }
}