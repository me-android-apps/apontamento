package com.definityanalytics.apontamento.ui.main

import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.SingleLiveEvent
import com.definityanalytics.apontamento.coreApplication.utils.coroutines.Coroutines
import com.definityanalytics.apontamento.coreApplication.utils.exception.ApiException
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.isDayBefore
import com.definityanalytics.apontamento.data.model.entities.*
import com.definityanalytics.apontamento.data.repositories.ApontamentoRepository
import com.definityanalytics.apontamento.data.repositories.CompanyRepository
import com.definityanalytics.apontamento.data.typeEnums.EnumStatusProcess
import com.definityanalytics.apontamento.data.typeEnums.EnumSyncStatusProcess
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class ViewModelApontamento(
    private val repoApontamento: ApontamentoRepository,
    private val companyRepository: CompanyRepository
) : ViewModel() {
    private val _loadingRequest = MutableLiveData<Boolean>()
    private val _listProcessApontamentos = MutableLiveData<ArrayList<Apontamento>>()
    private val _processApontamentosIsEmpty = MutableLiveData<Boolean>()
    private val _fragmentToStart = SingleLiveEvent<@IdRes Int>()
    private val _fragmentToStartLeft = SingleLiveEvent<@IdRes Int>()
    private val _onBackFrag = SingleLiveEvent<Boolean>()
    private val _showSnackBar = SingleLiveEvent<Int>()
    private var company: Company

    //add process
    private val _processApontamentoToAdd = MutableLiveData<Apontamento>()
    private val _listFragListGeneric = MutableLiveData<ArrayList<BaseEntitie>>()
    private var classOpenFragListGeneric: Class<*>? = null
    private val _openScannQrCode = SingleLiveEvent<Boolean>()
    private val _showErrorInput = MutableLiveData<Pair<@IdRes Int,@StringRes Int>>()
    private val _hideErrorInput = MutableLiveData<@IdRes Int>()
    var titleToolbarListGeneric: Int = 0
    var hasSubProcess = MutableLiveData<Boolean>(false)

    val loadingRequest: LiveData<Boolean>
        get() = _loadingRequest

    val processApontamentoToAdd: LiveData<Apontamento>
        get() = _processApontamentoToAdd

    val listProcessApontamentos: LiveData<ArrayList<Apontamento>>
        get() = _listProcessApontamentos

    val processApontamentosIsEmpty: LiveData<Boolean>
        get() = _processApontamentosIsEmpty

    val openScannQrCode: LiveData<Boolean>
        get() = _openScannQrCode

    val onBackFrag: LiveData<Boolean>
        get() = _onBackFrag

    val showSnackBar: LiveData<Int>
        get() = _showSnackBar

    val fragmentToStart: LiveData<Int>
        get() = _fragmentToStart

    val fragmentToStartLeft: LiveData<Int>
        get() = _fragmentToStartLeft

    //add process
    val listFragListGeneric: LiveData<ArrayList<BaseEntitie>>
        get() = _listFragListGeneric

    val showErrorInput: LiveData<Pair<Int,Int>>
        get() = _showErrorInput

    val hideErrorInput: LiveData<Int>
        get() = _hideErrorInput

    fun fetchApontamentosDb() {
        Coroutines.main {
            val response = repoApontamento.getAllApontamentos()
            _listProcessApontamentos.value = ArrayList(response ?: ArrayList())
            _processApontamentosIsEmpty.value = listProcessApontamentos.value.isNullOrEmpty()
        }
    }

    fun submitAddNewApontamento(view: View) {
        hasSubProcess.value = false
        _processApontamentoToAdd.value =
            Apontamento().apply {
                statusProcess.set(EnumStatusProcess.NS)
            }
        _fragmentToStart.value = R.id.proccessFragment
    }

    fun submitStartNewApontamento(view: View) {
        _processApontamentoToAdd.value?.apply {
            when (statusProcess.get()) {
                EnumStatusProcess.NS -> {
                    dateStart.set(Date())
                    statusProcess.set(EnumStatusProcess.ST)
                    Coroutines.main {
                        _processApontamentoToAdd.value?.apontamentoId =
                            repoApontamento.createApontamento(this)
                    }
                }

                EnumStatusProcess.ST -> {
                    _showSnackBar.value = R.string.process_already_started
                }
                EnumStatusProcess.SO -> {
                    _showSnackBar.value = R.string.process_already_stoped
                }
            }
        }
    }

    fun submitStopNewApontamento(view: View) {
        _processApontamentoToAdd.value?.apply {
            when (statusProcess.get()) {
                EnumStatusProcess.NS -> {
                    _showSnackBar.value = R.string.process_must_be_started
                }
                EnumStatusProcess.ST -> {
                    if(checkValuesRequired()){
                        dateStop.set(Date())
                        statusProcess.set(EnumStatusProcess.SO)
                        postRelease()
                    }
                }
                EnumStatusProcess.SO -> {
                    _showSnackBar.value = R.string.process_already_stoped
                }
            }
        }
    }

    private fun checkValuesRequired(): Boolean {
        var valuesCorrect = true
        if(processApontamentoToAdd.value?.employe?.get() == null){
            valuesCorrect = false
            _showErrorInput.value = Pair(R.id.txt_input_employee, R.string.employee_required)
        }else{
            _hideErrorInput.value = R.id.txt_input_employee
        }

        if(processApontamentoToAdd.value?.process?.get() == null){
            valuesCorrect = false
            _showErrorInput.value = Pair(R.id.txt_input_process, R.string.process_required)
        }else{
            _hideErrorInput.value = R.id.txt_input_process
        }

        if(processApontamentoToAdd.value?.process?.get()?.SubItem  == "S" && processApontamentoToAdd.value?.subProcess?.get() == null){
            valuesCorrect = false
            _showErrorInput.value = Pair(R.id.txt_input_sub_process, R.string.sub_process_required)
        }else{
            _hideErrorInput.value = R.id.txt_input_sub_process
        }

        if(processApontamentoToAdd.value?.barCode?.get() == null){
            valuesCorrect = false
            _showErrorInput.value = Pair(R.id.txt_input_bar_code, R.string.bar_code_required)
        }else{
            _hideErrorInput.value = R.id.txt_input_bar_code
        }

        return valuesCorrect

    }

    fun submitSyncApontamento(view: View) {
        _processApontamentoToAdd.value?.apply {
            postRelease()
        }
    }

    fun onfocusInputChange(view: View, hasFocus: Boolean) {
        if (hasFocus && _processApontamentoToAdd.value?.statusProcess?.get() != EnumStatusProcess.SO) {
            when (view.id) {
                R.id.edt_input_employee -> {
                    titleToolbarListGeneric = R.string.select_employee
                    classOpenFragListGeneric = User::class.java
                    _loadingRequest.value = true
                    fetchEmployees()
                    _fragmentToStart.value =
                        R.id.action_proccessFragment_to_listGenericFragment
                }
                R.id.edt_input_process -> {
                    titleToolbarListGeneric = R.string.select_process
                    classOpenFragListGeneric = Process::class.java
                    _loadingRequest.value = true
                    fetchProcess()
                    _fragmentToStart.value =
                        R.id.action_proccessFragment_to_listGenericFragment
                }
                R.id.edt_input_sub_process -> {
                    titleToolbarListGeneric = R.string.select_sub_process
                    classOpenFragListGeneric = SubProcess::class.java
                    _loadingRequest.value = true
                    fetchSubProcess()
                    _fragmentToStart.value =
                        R.id.action_proccessFragment_to_listGenericFragment
                }
//                R.id.edt_input_bar_code -> {
//                    _fragmentToStart.value =
//                        R.id.action_proccessFragment_to_barCodeFragment
//                }
            }
        }
    }

    fun itemFragListGenericSelected(view: View?, obj: BaseEntitie) {
        listFragListGeneric.value?.clear()
        when (obj) {
            is User -> {
                _processApontamentoToAdd.value?.employe?.set(obj)
                _hideErrorInput.value = R.id.txt_input_employee
            }
            is Process -> {
                _processApontamentoToAdd.value?.process?.set(obj)
                hasSubProcess.value = obj.SubItem == "S"
                _hideErrorInput.value = R.id.txt_input_process
                if(!hasSubProcess.value!!){
                    _processApontamentoToAdd.value?.subProcess?.set(null)
                    _hideErrorInput.value = R.id.txt_input_sub_process
                }
            }
            is SubProcess -> {
                _processApontamentoToAdd.value?.subProcess?.set(obj)
                _hideErrorInput.value = R.id.txt_input_sub_process
            }
        }
        _onBackFrag.value = true
    }

    fun itemListApontamentoSelected(view: View?, obj: BaseEntitie) {
        when (obj) {
            is Apontamento -> {
                _processApontamentoToAdd.value = obj
                hasSubProcess.value = obj.process.get()?.SubItem == "S"
                _fragmentToStartLeft.value =
                    R.id.action_listProcessFragment_to_proccessFragment
            }
        }
    }

    fun setCodeScann(codeScann: String) {
        _onBackFrag.postValue(true)
        val realCode = if(codeScann.length == 12) "0$codeScann" else codeScann
        processApontamentoToAdd.value?.barCode?.set(realCode)

        if(realCode.length == 13){
            _hideErrorInput.postValue(R.id.txt_input_bar_code)
        }else{
            _showErrorInput.postValue(Pair(R.id.txt_input_bar_code, R.string.bar_code_invalid))
        }
    }

    private fun fetchEmployees() {
        Coroutines.main {
            try {
                val response = repoApontamento.getEmployees(company.id ?: 0)
                _loadingRequest.value = false
                if (response?.isSuccessful ?: false) {
                    val listResponse = ArrayList<BaseEntitie>().apply {
                        response?.body()
                            ?.let { addAll(it.sortedBy { func -> func.baseToString() }) }
                    }

                    _listFragListGeneric.value = listResponse
                } else {

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                _loadingRequest.value = false
            }
        }
    }

    private fun fetchProcess() {
        Coroutines.main {
            try {
                val response = repoApontamento.getProcess(company.id ?: 0)
                _loadingRequest.value = false
                if (response?.isSuccessful ?: false) {
                    val listResponse =
                        ArrayList<BaseEntitie>().apply { response?.body()?.let { addAll(it) } }
                    _listFragListGeneric.value = listResponse
                } else {

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun fetchSubProcess() {
        Coroutines.main {
            try {
                val response = repoApontamento.getSubProcess(
                    _processApontamentoToAdd.value?.process?.get()?.ProcessoID ?: -1
                )
                _loadingRequest.value = false
                if (response?.isSuccessful ?: false) {
                    val listResponse =
                        ArrayList<BaseEntitie>().apply { response?.body()?.let { addAll(it) } }
                    _listFragListGeneric.value = listResponse
                } else {

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun postRelease() {
        _loadingRequest.value = true
        Coroutines.main {
            try {
                var responsePostRelease: Response<Unit>? = null
                _processApontamentoToAdd.value?.let {
                    repoApontamento.updateApontamento(it)
                    responsePostRelease = repoApontamento.postRelease(it)
                }
                _loadingRequest.value = false

                responsePostRelease?.let {
                    if (it.isSuccessful) {
                        _showSnackBar.value = R.string.post_release_success
                        checkBlockOptionsStopProcess()
                    } else {
                        _processApontamentoToAdd.value?.let { apontamento ->
                            apontamento.statusSyncProcess.set(EnumSyncStatusProcess.DS)
                            repoApontamento.updateApontamento(apontamento)
                        }
                        _showSnackBar.value = R.string.post_release_error
                    }
                }

            } catch (e: ApiException) {
                _loadingRequest.value = false
                _showSnackBar.value = R.string.post_release_error
                _processApontamentoToAdd.value?.let { apontamento ->
                    apontamento.statusSyncProcess.set(EnumSyncStatusProcess.DS)
                    repoApontamento.updateApontamento(apontamento)
                }
//                _showDialogError.value = Pair(R.string.key_company_invalid, R.string.check_key_company)

            } catch (e: Exception) {
                _loadingRequest.value = false
                _showSnackBar.value = R.string.post_release_error
                _processApontamentoToAdd.value?.let { apontamento ->
                    apontamento.statusSyncProcess.set(EnumSyncStatusProcess.DS)
                    repoApontamento.updateApontamento(apontamento)
                }
//                _showDialogError.value = Pair(R.string.title_error_auth, R.string.body_check_connect)

            }
        }
    }

    private fun checkBlockOptionsStopProcess() {
        val blockOptions = _processApontamentoToAdd.value?.blockOptions ?: false
        if (blockOptions) {
            _processApontamentoToAdd.value?.apply {
                apontamentoId = null
                barCode.set(null)
                statusProcess.set(EnumStatusProcess.NS)
                dateStart = ObservableField<Date>()
                dateStop = ObservableField<Date>()
                employe.set(processApontamentoToAdd.value?.employe?.get())
                process.set(processApontamentoToAdd.value?.process?.get())
                subProcess.set(_processApontamentoToAdd.value?.subProcess?.get())
                this.blockOptions = true
            }
        } else {
            _processApontamentoToAdd.value?.apply {
                apontamentoId = null
                barCode.set(null)
                statusProcess.set(EnumStatusProcess.NS)
                dateStart.set(null)
                dateStop.set(null)
                employe.set(null)
                process.set(null)
                subProcess.set(null)

            }
        }
    }

    init {
        company = companyRepository.getUserLogado()!!
    }

    @Suppress("UNCHECKED_CAST")
    class ApontamentoViewModelFactory(
        private val repository: ApontamentoRepository,
        private val companyRepository: CompanyRepository
    ) :
        ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ViewModelApontamento(repository, companyRepository) as T
        }
    }
}