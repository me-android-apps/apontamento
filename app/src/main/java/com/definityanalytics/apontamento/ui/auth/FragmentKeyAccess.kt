package com.definityanalytics.apontamento.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ActivityBase
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.databinding.FragmentKeyAccessBinding
import com.definityanalytics.apontamento.di.injectViewModel
import com.mebill.functionsgenericlibrary.showSnackBar
import org.kodein.di.generic.instance
import com.definityanalytics.apontamento.ui.auth.ViewModelAuth.AuthViewModelFactory


class FragmentKeyAccess : ScopedFragment() {
    private val factory : AuthViewModelFactory by instance<AuthViewModelFactory>()
    private lateinit var viewModelAuth : ViewModelAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentKeyAccessBinding.inflate(inflater,container,false)
        context ?: return binding.root

        viewModelAuth = activity?.injectViewModel(factory)!!

        binding.apply {
            authViewModel = viewModelAuth
            company = viewModelAuth.company.value
            initComponents(this)
            subScribeUi(this)
        }

        return binding.root
    }

    //COMPONENTS
    private fun subScribeUi(bindingFrag: FragmentKeyAccessBinding) {
        viewModelAuth.activityToStart.observe(activity!!, Observer { value ->
            val intent = Intent(activity, value.first)
            value.second?.let {
                intent.putExtras(it)
            }
            startActivity(intent)
            activity?.finish()
        })

        viewModelAuth.loadingRequest.observe(viewLifecycleOwner, Observer { loading ->
            if (loading && activity is ActivityBase) {
                (activity as ActivityBase).showDialogLoading()
            } else if(!loading && activity is ActivityBase){
                (activity as ActivityBase).dismissDialogLoading()
            }
        })

        viewModelAuth.showDialogError.observe(viewLifecycleOwner, Observer {
            it?.let {
                (activity as ActivityBase).showDialogError(it.first,it.second)
            }
        })

        viewModelAuth.setErrorKeyEmpty.observe(viewLifecycleOwner, Observer { errorEnable ->
            if(errorEnable) bindingFrag.apply {
                coordinatorAuthKeyAcess.showSnackBar(message = getString(R.string.set_key_company),colorRes = resources.getColor(R.color.colorGray))
            }
        })
    }

    private fun initComponents(bindingFrag: FragmentKeyAccessBinding){
        bindingFrag.apply {
//            btnSaveKey.setOnClickListener {
////                startActivity(Intent(activity,
////                    HostMainActivity::class.java))
//            }
        }
    }


}
