package com.definityanalytics.apontamento.ui.main

import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ActivityBase
import com.definityanalytics.apontamento.coreApplication.app.REQUEST_CAMERA_PERMISSION
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.navigateAnimToTop
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.navigateSingleTop
import com.definityanalytics.apontamento.databinding.ActivityMainBinding
import com.definityanalytics.apontamento.di.injectViewModel
import com.mebill.functionsgenericlibrary.setColorStateListCustom
import com.mebill.functionsgenericlibrary.startTranslationY
import kotlinx.android.synthetic.main.activity_main.*
import com.definityanalytics.apontamento.ui.main.ViewModelApontamento.ApontamentoViewModelFactory
import org.kodein.di.generic.instance

class ActivityHostMain : ActivityBase() {
    //viewModel
    private val factory : ApontamentoViewModelFactory by instance<ApontamentoViewModelFactory>()
    private lateinit var viewModelApontamento: ViewModelApontamento

    //Navigation
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModelApontamento = injectViewModel(factory)

        val bindingMain : ActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        bindingMain.apply {
            viewModel = viewModelApontamento
            subScribeUi(bindingMain)
            initComponents(bindingMain)
        }

        bindingMain.lifecycleOwner = this
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    // COMPONENTS
    private fun subScribeUi(bindingMain: ActivityMainBinding){
        viewModelApontamento.fragmentToStart.observe(this, Observer { idFrag ->
            navController.navigateAnimToTop(idFrag)
        })
    }

    private fun initComponents(bindingMain: ActivityMainBinding) {
        bindingMain.apply{
            //NAVIGATION
            navController = Navigation.findNavController(this@ActivityHostMain,R.id.nav_host_fragment_main)
            NavigationUI.setupWithNavController(bottomNavigationHome, navController)
            //menuAppBar
            onNavigationItemSelected()
            navController.addOnDestinationChangedListener { controller, destination, arguments ->
                when(destination.id){
                    R.id.proccessFragment, R.id.listGenericFragment, R.id.barCodeFragment ->{
                        this@ActivityHostMain.runOnUiThread {
                            bottomAppBar.startTranslationY(800F,400)
//                            Handler().postDelayed({bottomAppBar.visibility = View.GONE},400)
                            fabAddBill.hide()
                        }
                    }
                    R.id.settingsProfile ->{
                        this@ActivityHostMain.runOnUiThread {
                            fabAddBill.hide()
                        }
                    }
                    R.id.listProcessFragment -> {
                        this@ActivityHostMain.runOnUiThread {
                            fabAddBill.show()
                            bottomAppBar.apply {
                                visibility = View.VISIBLE
                                startTranslationY(0F, 600)
                            }
                        }
                    }
                }
            }

            //fab
            fabAddBill.apply {
                setColorStateListCustom(resources.getColor(R.color.colorWhite))

            }
        }

    }

    //LISTENERS
    private fun onNavigationItemSelected() {
        bottom_navigation_home?.setOnNavigationItemSelectedListener { item ->
            navController.navigateSingleTop(
                when (item.itemId) {
                    R.id.app_bar_home -> R.id.listProcessFragment
                    R.id.app_bar_profile -> R.id.settingsProfile
                    else -> R.id.listProcessFragment
                }
            )

            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d("REQUEST_PERMISSION", "$requestCode, $permissions, $grantResults")
        if(requestCode == REQUEST_CAMERA_PERMISSION){
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //PERMISION CAMERA ALOW
            } else {
                //PERMISSION CAMERA NOT ALOW
            }
        }
    }


}