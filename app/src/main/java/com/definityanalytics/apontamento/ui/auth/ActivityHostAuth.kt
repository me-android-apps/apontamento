package com.definityanalytics.apontamento.ui.auth

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ActivityBase
import com.definityanalytics.apontamento.databinding.ActivityHostAuthBinding
import com.definityanalytics.apontamento.di.injectViewModel
import org.kodein.di.generic.instance
import com.definityanalytics.apontamento.ui.auth.ViewModelAuth.AuthViewModelFactory

class ActivityHostAuth : ActivityBase() {
    private val factory : AuthViewModelFactory by instance<AuthViewModelFactory>()
    private lateinit var viewModelAuth : ViewModelAuth

    private var navController : NavController?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModelAuth = injectViewModel(factory)

        val binding : ActivityHostAuthBinding = DataBindingUtil.setContentView(this, R.layout.activity_host_auth)
        binding.lifecycleOwner = this
    }

    override fun onResume() {
        super.onResume()
        initComponents()
    }

    // COMPONENTS
    private fun initComponents(){
        //NAVIGATION
        navController = Navigation.findNavController(this,R.id.nav_host_fragment_auth)


    }
}