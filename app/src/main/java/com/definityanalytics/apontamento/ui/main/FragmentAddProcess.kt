package com.definityanalytics.apontamento.ui.main

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ActivityBase
import com.definityanalytics.apontamento.coreApplication.app.REQUEST_CAMERA_PERMISSION
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.navigateAnimToTop
import com.definityanalytics.apontamento.databinding.FragmentAddProcessBinding
import com.definityanalytics.apontamento.di.injectViewModel
import kotlinx.android.synthetic.main.fragment_add_process.*
import com.definityanalytics.apontamento.ui.main.ViewModelApontamento.ApontamentoViewModelFactory
import com.google.android.material.textfield.TextInputLayout
import com.mebill.functionsgenericlibrary.showSnackBar
import org.kodein.di.generic.instance

class FragmentAddProcess : ScopedFragment(), View.OnFocusChangeListener {
    //viewModel
    private val factory: ApontamentoViewModelFactory by instance<ApontamentoViewModelFactory>()
    private lateinit var viewModelApontamento: ViewModelApontamento

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bindingFrag = FragmentAddProcessBinding.inflate(inflater, container, false)
        context ?: return bindingFrag.root
        bindingFrag.lifecycleOwner = this

        viewModelApontamento = activity?.injectViewModel(factory)!!

        bindingFrag.apply {
            viewModel = viewModelApontamento
            apontamento = viewModelApontamento.processApontamentoToAdd.value
            subScribeUi(this)
            initComponents(this)
        }

        return bindingFrag.root
    }

    //COMPONENTS
    private fun subScribeUi(bindingFrag: FragmentAddProcessBinding) {
        viewModelApontamento.apply {
            fragmentToStart.observe(viewLifecycleOwner, Observer { idFrag ->
                Navigation.findNavController(view!!).navigateAnimToTop(idFrag)
            })

            showSnackBar.observe(viewLifecycleOwner, Observer { idResourceString ->
                bindingFrag.apply {
                    coordinatorAddProcess.showSnackBar(message = getString(idResourceString),colorRes = resources.getColor(R.color.colorGray))
                }
            })

            openScannQrCode.observe(viewLifecycleOwner, Observer { openScannQrCode ->
                if(openScannQrCode)showScannBarCode()
            })

            loadingRequest.observe(viewLifecycleOwner, Observer { loading ->
                if (loading && activity is ActivityBase) {
                    (activity as ActivityBase).showDialogLoading()
                } else if(!loading && activity is ActivityBase){
                    (activity as ActivityBase).dismissDialogLoading()
                }
            })

            showErrorInput.observe(viewLifecycleOwner, Observer { pairInputError ->
                showErrorInput(pairInputError.first,pairInputError.second)
            })

            hideErrorInput.observe(viewLifecycleOwner, Observer { idInputHideError ->
                hideErrorInput(idInputHideError)
            })
        }
    }

    private fun showErrorInput(idResourceInput: Int,idResourceString: Int){
        val inputAux = view?.findViewById<TextInputLayout>(idResourceInput)
        inputAux?.let {
            val message = resources.getString(idResourceString)
            it.error = message
            it.isErrorEnabled = true
        }
    }

    private fun hideErrorInput(idInputHideError: Int) {
        val inputAux = view?.findViewById<TextInputLayout>(idInputHideError)
        inputAux?.let {
            it.error = null
            it.isErrorEnabled = false
        }
    }

    private fun initComponents(bindingFrag: FragmentAddProcessBinding?) {
        bindingFrag?.apply {
            edtInputBarCode.onFocusChangeListener = this@FragmentAddProcess

            tbAddProcess.setNavigationOnClickListener { activity?.onBackPressed() }
        }
    }

    //LISTENERS
    override fun onFocusChange(view: View?, hasFocus: Boolean) {
        if (hasFocus) {
            when (view?.id) {
                R.id.edt_input_bar_code -> showScannBarCode()
            }
        }
    }

    private fun showScannBarCode() {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            Navigation.findNavController(edt_input_bar_code)
                .navigateAnimToTop(R.id.action_proccessFragment_to_barCodeFragment)
        } else {
            edt_input_bar_code.clearFocus()
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_CAMERA_PERMISSION
            )
        }
    }
}