package com.definityanalytics.apontamento.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.util.isNotEmpty
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.removeTranslucentStatusBar
import com.definityanalytics.apontamento.databinding.FragmentAddProcessBinding
import com.definityanalytics.apontamento.databinding.FragmentBarCodeBinding
import com.definityanalytics.apontamento.di.injectViewModel
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.mebill.functionsgenericlibrary.setTranslucentStatusBar
import com.mebill.functionsgenericlibrary.vibratePhone
import com.definityanalytics.apontamento.ui.main.ViewModelApontamento.ApontamentoViewModelFactory
import org.kodein.di.generic.instance
import java.io.IOException

class FragmentBarCode : ScopedFragment() {
    //viewModel
    private val factory : ApontamentoViewModelFactory by instance<ApontamentoViewModelFactory>()
    private lateinit var viewModelApontamento: ViewModelApontamento

    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private val REQUEST_CAMERA_PERMISSION = 201

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bindingFrag = FragmentBarCodeBinding.inflate(inflater,container,false)
        context?:return bindingFrag.root
        bindingFrag.lifecycleOwner = this

        viewModelApontamento = activity?.injectViewModel(factory)!!

        bindingFrag.apply {
            viewModel = viewModelApontamento
            subScribeUi(this)
            initialiseDetectorsAndSources(this)
        }

        return bindingFrag.root
    }

    override fun onPause() {
        super.onPause()
        try {
            cameraSource?.release()
        }catch (ex : Exception){
            ex.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.setTranslucentStatusBar()
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.removeTranslucentStatusBar()
    }

    //COMPONENTS
    private fun subScribeUi(bindingFrag: FragmentBarCodeBinding?){
        viewModelApontamento.onBackFrag.observe(viewLifecycleOwner, Observer { onBack ->
            activity?.onBackPressed()
        })
    }
    private fun initialiseDetectorsAndSources(bindingFrag: FragmentBarCodeBinding) {
        var firstDetectionBarCode = true
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            barcodeDetector = BarcodeDetector.Builder(activity)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build()

            cameraSource = CameraSource.Builder(activity, barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true)
                .build()


            bindingFrag.surfaceViewBarCode.visibility = View.VISIBLE
            bindingFrag.surfaceViewBarCode.holder.addCallback(object : SurfaceHolder.Callback {
                override fun surfaceChanged(
                    holder: SurfaceHolder,
                    format: Int,
                    width: Int,
                    height: Int){}

                override fun surfaceDestroyed(holder: SurfaceHolder) {
                    cameraSource?.stop()
//                    if (cameraSource != null) {
//                        cameraSource?.release();
//                        cameraSource = null;
//                    }
                }

                @SuppressLint("MissingPermission")
                override fun surfaceCreated(holder: SurfaceHolder) {
                    try {
                        cameraSource?.start(bindingFrag.surfaceViewBarCode.holder)

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
            })

            barcodeDetector?.setProcessor(object : Detector.Processor<Barcode> {
                override fun release() {

                }

                override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
                    detections?.detectedItems?.let {
                        val barcodes: SparseArray<Barcode> = detections.detectedItems
                        if (barcodes.isNotEmpty() && firstDetectionBarCode) {
                            firstDetectionBarCode = false
                            barcodes.valueAt(0)?.let {
                                it.rawValue?.let { raw ->

                                    try {
                                        Log.d("QR CODE", "value: $raw")
                                        activity?.vibratePhone()
                                        viewModelApontamento.setCodeScann(codeScann = raw)
                                    }catch (ex : Exception){
                                        ex.printStackTrace()
                                    }
                                }
                            }
                        }
                    }
                }
            })
        } else {
            Toast.makeText(activity,getString(R.string.permission_camera_required),Toast.LENGTH_LONG).show()
        }
    }

}