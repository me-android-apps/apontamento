package com.definityanalytics.apontamento.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.navigateAnimToLeft
import com.definityanalytics.apontamento.coreApplication.utils.ui.adapter.AdapterGeneric
import com.definityanalytics.apontamento.data.repositories.CompanyRepository
import com.definityanalytics.apontamento.data.typeEnums.EnumMenuSettings
import com.definityanalytics.apontamento.databinding.FragmentProfileBinding
import com.definityanalytics.apontamento.di.injectViewModel
import com.definityanalytics.apontamento.ui.auth.ViewModelAuth
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder
import org.kodein.di.generic.instance
import com.definityanalytics.apontamento.ui.auth.ViewModelAuth.AuthViewModelFactory

class FragmentSettingsProfile : ScopedFragment() {
    private val factory : AuthViewModelFactory by instance<AuthViewModelFactory>()
    private lateinit var viewModelAuth : ViewModelAuth
    private lateinit var adapterItensSettings: AdapterGeneric<Pair<EnumMenuSettings, Int>>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bindingFrag = FragmentProfileBinding.inflate(inflater, container, false)
        context ?: return bindingFrag.root

        viewModelAuth = activity?.injectViewModel(factory)!!

        bindingFrag.apply {
            subScribeUi(this)
            initComponents(bindingFrag)
        }

        return bindingFrag.root
    }

    //COMPONENTS
    private fun subScribeUi(bindingFrag: FragmentProfileBinding) {
        viewModelAuth.activityToStart.observe(viewLifecycleOwner, Observer { value ->
            val intent = Intent(activity, value.first)
            value.second?.let {
                intent.putExtras(it)
            }
            startActivity(intent)
            activity?.finish()
        })
    }

    private fun initComponents(bindingFrag: FragmentProfileBinding) {
        bindingFrag.apply {
            keyCompany = viewModelAuth.keyCompany

            rcItensSettings.apply {
                adapterItensSettings = AdapterGeneric(
                    layoutId = R.layout.item_list_settings,
                    onItemListClicked = ::funAux
                )
                adapter = adapterItensSettings
                adapterItensSettings.submitList(
                    arrayListOf(
                        Pair(EnumMenuSettings.SO, R.drawable.ic_about),
                        Pair(EnumMenuSettings.SA, R.drawable.ic_power_switch)
                    )
                )
            }
        }

    }

    private fun funAux(view: View?, menu: Pair<EnumMenuSettings, Int>) {
        when(menu.first){
            EnumMenuSettings.SA -> viewModelAuth.logoutUser()
            EnumMenuSettings.PP -> Toast.makeText(activity,getText(R.string.privace_police),Toast.LENGTH_SHORT).show()
            EnumMenuSettings.SO -> {
                val fragment = LibsBuilder()
                    .withVersionShown(false)
                    .withLicenseShown(true)
                    .withAboutIconShown(true)
                    .withAboutVersionShown(true)
                    .withFields(R.string::class.java.fields) // in some cases it may be needed to provide the R class, if it can not be automatically resolved
                    .withAboutDescription("<b>Copyright 2020 Definity Analytics</b> <br /> Abaixo estão listadas as bibliotecas open source usadas neste aplicativo.")
                    .withLibraryModification("aboutlibraries", Libs.LibraryFields.LIBRARY_NAME, "AboutLibraries")
                    .supportFragment()

                findNavController().navigateAnimToLeft(R.id.about_fragment,fragment.arguments)

            }
        }
    }
}