package com.definityanalytics.apontamento.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.definityanalytics.apontamento.R
import com.definityanalytics.apontamento.coreApplication.app.ScopedFragment
import com.definityanalytics.apontamento.coreApplication.utils.extensionFunction.navigateAnimToLeft
import com.definityanalytics.apontamento.coreApplication.utils.ui.adapter.AdapterGeneric
import com.definityanalytics.apontamento.data.model.entities.Apontamento
import com.definityanalytics.apontamento.databinding.FragmentListProcessBinding
import com.definityanalytics.apontamento.di.injectViewModel
import org.kodein.di.generic.instance
import com.definityanalytics.apontamento.ui.main.ViewModelApontamento.ApontamentoViewModelFactory

class FragmentListProcess : ScopedFragment() {
    //viewModel
    private val factory : ApontamentoViewModelFactory by instance<ApontamentoViewModelFactory>()
    private lateinit var viewModelApontamento: ViewModelApontamento

    private lateinit var adapterItensProcess : AdapterGeneric<Apontamento>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentListProcessBinding.inflate(inflater,container,false)
        context ?: return binding.root
        binding.lifecycleOwner = this

        viewModelApontamento = activity?.injectViewModel(factory)!!

        binding.apply {
            processIsEmpty = viewModelApontamento.processApontamentosIsEmpty.value
            subScribeUi(this)
            initComponents(this)
        }

        viewModelApontamento.fetchApontamentosDb()
        return binding.root
    }

    //COMPONENTS
    private fun subScribeUi(binding : FragmentListProcessBinding){
        viewModelApontamento.fragmentToStartLeft.observe(viewLifecycleOwner, Observer { idFrag ->
            Navigation.findNavController(view!!).navigateAnimToLeft(idFrag)
        })

        viewModelApontamento.listProcessApontamentos.observe(viewLifecycleOwner, Observer { list ->
            list?.let {
                adapterItensProcess.submitList(it)
            }
        })

        viewModelApontamento.processApontamentosIsEmpty.observe(viewLifecycleOwner, Observer { isEmpty ->
            binding.processIsEmpty = isEmpty
        })
    }
    private fun initComponents(binding : FragmentListProcessBinding){
        binding.apply {
            rcItensProcess.apply {
                adapterItensProcess = AdapterGeneric(
                    layoutId = R.layout.item_list_process,
                    onItemListClicked = viewModelApontamento::itemListApontamentoSelected)
                adapter = adapterItensProcess
            }
        }
    }

    private fun funAux(view:View?,apontamento: Apontamento) {
        view?.let { Navigation.findNavController(it).navigateAnimToLeft(R.id.action_listProcessFragment_to_proccessFragment) }
    }
}