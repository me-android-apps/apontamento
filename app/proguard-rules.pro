# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
#-dontwarn org.codehaus.jackson.**
#-keep class org.codehaus.jackson.**
#-keepattributes Exceptions
#
#-keep class com.implantacao.brazzo.implantacaoapp.DAO.database.CrateDB.**
#-keepclassmembers class com.implantacao.brazzo.implantacaoapp.DAO.database.CrateDB.** { *; }
#
#-keep class com.implantacao.brazzo.implantacaoapp.DAO.repositorios.Repo_login.**
#-keepclassmembers class com.implantacao.brazzo.implantacaoapp.DAO.repositorios.Repo_login.** { *; }
#
#-keep class com.implantacao.brazzo.implantacaoapp.DAO.**
#-keepclassmembers class com.implantacao.brazzo.implantacaoapp.DAO.entidadeCL** { *; }
#
#-keep class org.sqlite.** { *; }
#-keep class org.sqlite.database.** { *; }

-optimizationpasses 5
-dontpreverify
-repackageclasses ''
-allowaccessmodification
-optimizations !code/simplification/arithmetic
-keepattributes *Annotation*

-verbose

-dump obfuscation/class_files.txt
-printseeds obfuscation/seeds.txt
-printusage obfuscation/unused.txt
-printmapping obfuscation/mapping.txt

-keep class * extends androidx.room.RoomDatabase
-dontwarn androidx.room.paging.**

-keep class * extends androidx.fragment.app.Fragment{}
-keepnames class androidx.navigation.fragment.NavHostFragment

-keep class *.R
-keep class **.R$* {
    <fields>;
}
-keepclasseswithmembers class **.R$* {
    public static final int define_*;
}

# class models
-keepclassmembers class com.definityanalytics.apontamento.data.dao** { *; }
-keepclassmembers class com.definityanalytics.apontamento.data.db** { *; }
-keepclassmembers class com.definityanalytics.apontamento.data.model.entities** { *; }
-keepclassmembers class com.definityanalytics.apontamento.data.model.local** { *; }
-keepclassmembers class com.definityanalytics.apontamento.data.model.server** { *; }
-keepclassmembers class com.definityanalytics.apontamento.data.typeEnums** { *; }
